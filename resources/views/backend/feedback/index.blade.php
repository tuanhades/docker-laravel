@extends('backend.layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p></p>
                @if(session('flash_message'))
                    <div class="alert alert-success" role="alert">
                        {{session('flash_message')}}
                    </div>
                @endif
                <div class="card">

                    <div class="card-body">

                        <div class="table-responsive mt15">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Subject</th>
                                    <th class="text-center">View</th>
                                    <th class="text-center">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($listFeedback->total() > 0)
                                    @foreach($listFeedback as $item)
                                        <tr>
                                            <td class="text-center">{{ $item->id }}</td>
                                            <td class="text-center">{{ $item->name }}</td>
                                            <td class="text-center">{{ $item->subject }}</td>
                                            <td class="text-center">
                                                <a class="btn btn-sm btn-warning" href="{{ route('backend.feedback.show', $item->id) }}">View</a>
                                            </td>
                                            <td class="text-center">
                                                <form method="POST" class="form-delete"
                                                      action="{{ route('backend.feedback.destroy', $item->id) }}"
                                                      accept-charset="UTF-8" style="display:inline">
                                                    {{csrf_field()}}
                                                    {{ method_field('DELETE') }}
                                                    <button type="submit" class="btn btn-sm btn-danger js-delete-btn">
                                                        Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7">No data</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        @if($listFeedback->total() > 0)
                            @include('common.paginator', ['paginator' => $listFeedback])
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
