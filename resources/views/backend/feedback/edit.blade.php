@extends('backend.layouts.master')

@section('title')
    Edit
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
<p></p>
                <div class="card">

                    <div class="card-body">
                        <form class="form-horizontal js-form-validate" action="{{ route('backend.feedback.update', $feedback->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            @include('backend.feedback._form', ['item' => $feedback])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
