@extends('frontend.layouts.master')

@section('title')
    Create
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
<p></p>
                <div class="card">
                    <div class="card-body">
                        <form class="form-horizontal js-form-validate" action="{{ route('frontend.feedback.store') }}" method="POST">
                            @csrf
                            @include('frontend.feedback._form', ['item' => optional(null) ])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
