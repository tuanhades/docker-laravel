@extends('frontend.layouts.master')

@section('title')
    Show
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
<p></p>
                @if(session('flash_message'))
                    <div class="alert alert-success" role="alert">
                        {{session('flash_message')}}
                    </div>
                @endif
                <div class="card">

                    <div class="card-body">
                        <form class="form-horizontal js-form-validate" action="{{route('frontend.feedback.create_reply')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="title" class="col-md-6 control-label">Name : {{$feedback->name}}</label>
                                <input name="feedback_id" type="hidden" value="{{ $feedback->id }}">
                                <input name="type" type="hidden" value="2">
                            </div>
                            <div class="form-group">
                                <label for="title" class="col-md-12 control-label">Subject: {{$feedback->subject}}</label>
                            </div>
                            <div class="form-group">
                                <label for="title" class="col-md-12 control-label">Message: {!! nl2br($feedback->message) !!}</label>
                            </div>
                            @if(count($feedback->reply()->get()->toArray()) > 0)
                                <hr />
                                @foreach($feedback->reply()->get() as $value)
                                    <small>@if($value->type == 1) Reply @else Feedback @endif</small>
                                    <div class="card">
                                        <div class="card-body">
                                            <small>{{$value->created_at}}</small>
                                            <h5 class="card-title">{{$value->name}}</h5>
                                            <p class="card-text">{!! nl2br($value->message) !!}</p>

                                        </div>
                                    </div>
                                @endforeach
                            @endif

                            <hr>

                            <div class="form-group">
                                <label for="title" class="col-md-12 control-label"><span class="text-red">※</span>Name</label>
                                <div class="col-md-12 @if($errors->has('name')) has-error @endif">
                                    <input class="form-control" type="text" name="name" value="{{ old('name', $feedback->name) }}">
                                </div>
                                @if($errors->has('name'))
                                    <p class="text-danger">{{$errors->first('name')}}</p>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="col-md-12 control-label">Message</label>
                                <div class="col-md-12">
                                    <textarea class="form-control" name="message" rows="4">{{ old('message') }}</textarea>
                                </div>
                            </div>

                            <p></p>

                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <a href="{{ route('backend.feedback.index') }}" title="Back">
                                        <button class="btn btn-default" type="button">Back</button>
                                    </a>
                                    <button class="btn btn-primary" type="submit">Feedback</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

