<div class="form-group">
    <label for="title" class="col-md-12 control-label"><span class="text-red">※</span>Name</label>
    <div class="col-md-12 @if($errors->has('subject')) has-error @endif">
        <input class="form-control" type="text" name="name" value="{{ old('name', $item->name) }}">
        <input name="id" type="hidden" value="{{ $item->id }}">
    </div>
    @if($errors->has('name'))
        <p class="text-danger">{{$errors->first('name')}}</p>
    @endif
</div>

<div class="form-group">
    <label for="title" class="col-md-12 control-label"><span class="text-red">※</span>Subject</label>
    <div class="col-md-12 @if($errors->has('subject')) has-error @endif">
        <input class="form-control" name="subject" type="text" value="{{ old('subject', $item->subject) }}">
    </div>
    @if($errors->has('subject'))
        <p class="text-danger">{{$errors->first('subject')}}</p>
    @endif
</div>

<div class="form-group">
    <label class="col-md-12 control-label">Message</label>
    <div class="col-md-12">
        <textarea class="form-control" name="message" rows="4">{{ old('message', nl2br($item->message)) }}</textarea>
    </div>
</div>

<p></p>

<div class="form-group">
    <div class="col-md-12 text-center">
        <a href="{{ route('frontend.feedback.index') }}" title="Back">
            <button class="btn btn-default" type="button">Back</button>
        </a>
        <button class="btn btn-primary" type="submit">@if($item->id) Update @else Add @endif</button>
    </div>
</div>

