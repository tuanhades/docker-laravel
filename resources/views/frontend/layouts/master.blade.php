<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>@yield('title', 'Frontend')</title>
	<link rel="icon" href="{{ asset('favicon.ico') }}">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">


    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min') }}"></script>
    @yield('style')

</head>
<body>

	@include("frontend.common.header")
    <div class="container">
    @yield('content')
    </div>

    @include("frontend.common.footer")

    <script>
        $('form').submit(function() {
            $(this).find('button[type="submit"]').prop('disabled', true);
        });
    </script>
    @yield('script')
</body>
</html>
