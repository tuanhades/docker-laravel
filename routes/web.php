<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('frontend.')->namespace('Frontend')->group(function() {

    Route::get('/', 'FrontendController@index')->name('home');
    Route::post('/feedback/reply', 'FeedbackController@create_reply')->name('feedback.create_reply');
    Route::resource('feedback', 'FeedbackController');


});
