<?php

namespace App\Repositories;

use App\Models\Reply;
use Illuminate\Support\Facades\DB;

class ReplyRepository extends EloquentRepository
{
    public function getModel(): String
    {
        return Reply::class;
    }

    public function getAll() {
        return $this->model->get();
    }
}
