<?php

namespace App\Repositories;

use App\Models\Feedback;
use Illuminate\Support\Facades\DB;

class FeedbackRepository extends EloquentRepository
{
    public function getModel(): String
    {
        return Feedback::class;
    }

    public function getAll() {
        return $this->model->get();
    }

    public function customDestroy($id) {
        $feedback = $this->model->find($id);
        $feedback->reply()->delete();
        $feedback->delete();
        return true;
    }
}
