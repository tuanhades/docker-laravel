<?php

namespace App\Repositories;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Repositories\Traits\Sortable;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Traits\Relationable;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

abstract class EloquentRepository implements RepositoryInterface
{

    use Sortable, Relationable;

    protected $model;

    protected $table;

    protected $paginate = 20;

    protected $owner_of_cemetery;

    abstract public function getModel(): string;

    public function __construct()
    {
        $this->setModel();
        $this->table = $this->model->getTable();
    }

    public function setModel(): void
    {
        $this->model = app()->make($this->getModel());
    }

    public function all(): Collection
    {
        return $this->model
            ->with($this->relations)
            ->orderBy($this->sortBy, $this->sortOrder)->get();
    }

    public function paginated($paginate = null): LengthAwarePaginator
    {
        $paginate = $paginate ?: $this->paginate;
        $resuilt = $this->model
            ->orderBy($this->sortBy, $this->sortOrder)
            ->paginate($paginate);
        return $resuilt;
    }

    public function search($paginate = null, $with = [], $condition = []): LengthAwarePaginator
    {
        $paginate = $paginate ?: $this->paginate;
        $resuilt = $this->model->with($with)
            ->where($condition)
            ->orderBy($this->sortBy, $this->sortOrder)
            ->paginate($paginate);
        return $resuilt;
    }

    public function getTable()
    {
        return $this->model->getTable();
    }

    /**
     * Base create
     *
     * @param array $input
     * @return Model
     */
    public function create($input): Model
    {
        return $this->model->create($input);
    }

    /**
     * Get by id
     *
     * @param [type] $id
     * @return Model
     */
    public function get($id, $withTrashed = false): ?Model
    {
        if ($withTrashed) {
            return $this->model->where('id', $id)->withTrashed()->first();
        }
        return $this->model->where('id', $id)->first();
    }

    /**
     * Get first by conditions
     *
     * @param array $condition
     * @return void
     */
    public function firstBy($condition, $value = '')
    {
        if ($condition) {
            if (is_array($condition)) {
                return $this->model->where(is_array(array_values($condition)) ? $condition : [$condition])->first();
            }
            return $this->model->where($condition, $value)->first();
        }
        return null;
    }

    /**
     * Get list by foreign key
     *
     * @param [type] $id
     * @return Model
     */
    public function getListByForeignKey($foreignKey, $value): Collection
    {
        return $this->model->where($foreignKey, $value)->get();
    }

    public function firstOrNew($id)
    {
        return $this->model->firstOrNew(['id' => $id]);
    }

    /**
     * Get by list ids
     *
     * @param array $ids
     * @return Collection
     */
    public function getByIds(array $ids): Collection
    {
        if (!empty($ids)) {
            return $this->model->whereIn('id', $ids)->get();
        }
        return collect([]);
    }

    public function getOrFail($id): ?Model
    {
        return $this->model->where('id', $id)->firstOrFail();
    }

    /**
     * Base handle update
     *
     * @param int $id
     * @param array $input
     * @return Model|null
     */
    public function update($id, array $input): ?Model
    {
        $model = $this->model->find($id);
        if ($model) {
            $model->fill($input);
            $model->save();
        } else {
            log_warning("Update unknown id: {$id}, data: " . json_encode($input));
        }
        return $model;
    }

    public function destroy($id)
    {
        return $this->model->destroy($id);
    }

    public function forceDelete($id)
    {
        return $this->model->destroy($id);
    }

    public function forceDeleteBy($column, $value)
    {
        return $this->model->where($column, $value)->forceDelete();
    }

    public function with($relations): Model
    {
        return $this->model->with($relations);
    }


    public function escape($inp)
    {
        if(!empty($inp) && is_string($inp)) {
            $reg         = array('\\', "\0", "\n", "\r", "'", '"', "\x1a");
            $reg_replace = array('', '', '', '', "", '', '');
            $inp = str_replace($reg, $reg_replace, trim($inp));

            return $inp;
        }

        return "";
    }

    public function dateDashToSlash($date)
    {
        $date = $this->escape($date, "date");
        if (!empty($date)) {
            $date = str_replace("/", "-", substr($date, 0, 10));
        }

        return $date;
    }

    public function dateSlashToDash($date)
    {
        $date = $this->escape($date);
        if (!empty($date)) {
            $date = str_replace("-", "/", substr($date, 0, 10));
        }

        return $date;
    }

    public function makeWhere($where , $key , $value1 = "" , $value2 = "" , $div = "=", $type = "")
    {
        $value1 = is_array($value1) ? $value1 : trim($value1);
        $value2 = is_array($value2) ? $value2 : trim($value2);

        $len1 = is_array($value1) ? count($value1) : trim($value1);
        $len2 = is_array($value2) ? count($value2) : trim($value2);

        if ($len1 > 0 || $len2 > 0) {
            $div = strtolower($div);
            $type = strtolower($type);
            if ($div == "in") {
                $convert_value1 = $value1;
                $convert_value2 = $value2;
            } elseif ($type == 'int') {
                $convert_value1 = (int)$value1;
                $convert_value2 = (int)$value2;
            } else {
                $convert_value1 = $this->escape($value1);
                $convert_value2 = $this->escape($value2);
            }

            switch ($div) {
                case "like":
                    $where[] = [$key, "LIKE", "%$convert_value1%"];
                    break;
                case "in":
                    $where[] = [$key, $convert_value1];
                    break;
                case "fromto":
                    if (strlen($value1) > 0) {
                        $where[] = [$key, ">=", $convert_value1];
                    }
                    if (strlen($value2) > 0) {
                        $where[] = [$key, "<=", $convert_value2];
                    }
                    break;
                default:
                     $where[] = [$key, $div, $convert_value1];
                    break;
            }
        }

        return $where;
    }

    public function makeWhereLike($condition, $key, $value)
    {
        return $this->makeWhere($condition, $key, $value, '', 'like');
    }

    public function addComma($value) {
        $value = str_replace(',' , '' , $value);

        $arr_data = explode ( ".", $value);

        $t_int = number_format((int)$arr_data[0]);
        $t_dec = isset($arr_data[1]) ? "." . $arr_data[1] : "";

        $return_data =  $t_int . $t_dec;

        return $return_data;
    }

    public function delComma($value, $type = 'int') {
        $return_data = "";

        if ($value) {
            $return_data = str_replace(',' , '' , $value);
        }

        if ($type == 'double') {
            return (double)$return_data;
        }

        return (int)$return_data;
    }
}
