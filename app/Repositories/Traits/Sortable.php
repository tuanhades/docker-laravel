<?php

namespace App\Repositories\Traits;

trait Sortable
{
    public $sortBy = 'id';

    public $sortOrder = 'asc';

    public function setSortBy($sortBy = 'id')
    {
        $this->softBy = $sortBy;
    }

    public function setSortOrder($sortOrder = 'desc')
    {
        $this->sortOrder = $sortOrder;
    }
}
