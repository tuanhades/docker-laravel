<?php

namespace App\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface RepositoryInterface
{
    public function getModel(): string;

    public function all(): Collection;

    public function paginated($paginate): LengthAwarePaginator;

    public function create($input): Model;

    public function get($id): ?Model;

    public function getOrFail($id): ?Model;

    public function update($id, array $input): ?Model;
}
