<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReplyRequest;
use App\Repositories\FeedbackRepository;
use App\Repositories\ReplyRepository;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{

    public function __construct(FeedbackRepository $feedbackRepository, ReplyRepository $replyRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
        $this->replyRepository = $replyRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $listFeedback = $this->feedbackRepository->paginated();
        return view('backend.feedback.index', compact('listFeedback'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.feedback.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ReplyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReplyRequest $request)
    {
        $this->replyRepository->create($request->input());

        return redirect()->back()->with('flash_message', __('Reply Success'));
    }

    /**
     * Show the form for preview the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $feedback = $this->feedbackRepository->firstBy(['id' => $id]);
        if(!$feedback) return redirect()->route('backend.feedback.index');

        return view('backend.feedback.show', compact('feedback'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feedback = $this->replyRepository->firstBy(['id' => $id]);
        if(!$feedback) return redirect()->route('backend.feedback.index');

        return view('backend.feedback.edit', compact('feedback'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ReplyRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ReplyRequest $request, $id)
    {
        $data = $request->input();
        $this->replyRepository->update($id, $data);

        return redirect()->route('backend.feedback.index')->with('flash_message', __('Update Success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->feedbackRepository->customDestroy($id)) {
            return redirect()->back()->with('flash_message', __('Delete Success'));
        }
        return redirect()->back();
    }

}
