<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeedbackRequest;
use App\Http\Requests\ReplyRequest;
use Illuminate\Http\Request;
use App\Repositories\FeedbackRepository;
use App\Repositories\ReplyRepository;

class FeedbackController extends Controller
{

    public function __construct(FeedbackRepository $feedbackRepository, ReplyRepository $replyRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
        $this->replyRepository = $replyRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $listFeedback = $this->feedbackRepository->paginated();
        return view('frontend.feedback.index', compact('listFeedback'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.feedback.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\FeedbackRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FeedbackRequest $request)
    {
        $this->feedbackRepository->create($request->input());

        return redirect()->route('frontend.feedback.index')->with('flash_message', __('Create Success'));
    }

    /**
     * Store a newly created reply resource in storage.
     *
     * @param  \Illuminate\Http\ReplyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function create_reply(ReplyRequest $request)
    {
        $this->replyRepository->create($request->input());

        return redirect()->back()->with('flash_message', __('Feedback Success'));
    }

    /**
     * Show the form for preview the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $feedback = $this->feedbackRepository->firstBy(['id' => $id]);
        if(!$feedback) return redirect()->route('frontend.feedback.index');

        return view('frontend.feedback.show', compact('feedback'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feedback = $this->feedbackRepository->firstBy(['id' => $id]);
        if(!$feedback) return redirect()->route('frontend.feedback.index');

        return view('frontend.feedback.edit', compact('feedback'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\FeedbackRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FeedbackRequest $request, $id)
    {
        $data = $request->input();
        $this->feedbackRepository->update($id, $data);

        return redirect()->route('frontend.feedback.index')->with('flash_message', __('Update Success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->feedbackRepository->customDestroy($id)) {
            return redirect()->back()->with('flash_message', __('Delete Success'));
        }
        return redirect()->back();
    }

}
